import { Express, Request, Response } from "express";
import { Result } from "../Lib/App/Result";
import { MoviesService } from "../Services/MoviesService";

export namespace MoviesController {
  export async function getMovies(req: Request, res: Response): Promise<void> {
    const result: Result = await MoviesService.getMovies();
    if (result.isSuccessful) {
      res.status(201).send(result.data);
    } else {
      res.status(400).send({ errors: result.errors });
    }
  }

  export async function acquireMovies(
    req: Request,
    res: Response
  ): Promise<void> {
    const result: Result = await MoviesService.saveMovies(req.body);
    if (result.isSuccessful) {
      res.status(201).send(result.data);
    } else {
      res.status(400).send({ errors: result.errors });
    }
  }
}
