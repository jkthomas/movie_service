import { Express, Request, Response } from "express";
import { Result } from "../Lib/App/Result";
import { CommentsService } from "../Services/CommentsService";

export namespace CommentsController {
  export async function getComments(
    req: Request,
    res: Response
  ): Promise<void> {
    const result: Result = await CommentsService.getComments();
    if (result.isSuccessful) {
      res.status(201).send(result.data);
    } else {
      res.status(400).send({ errors: result.errors });
    }
  }

  export async function addComment(req: Request, res: Response): Promise<void> {
    const result: Result = await CommentsService.saveComment(req.body);
    if (result.isSuccessful) {
      res.status(201).send(result.data);
    } else {
      res.status(400).send({ errors: result.errors });
    }
  }
}
