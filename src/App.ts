import express, { Express } from "express";
import path from "path";
import passport from "passport";
import flash from "express-flash";
import bodyParser from "body-parser";
import lusca from "lusca";
import morgan from "morgan";
import { MoviesRouter } from "./Routes/MoviesRouter";
import { CommentsRouter } from "./Routes/CommentsRouter";
import { connect } from "mongoose";
import dotenv from "dotenv";

const App: Express = express();

dotenv.config();
App.set("port", process.env.PORT || 4000);
App.use(passport.initialize());
App.use(passport.session());
App.use(flash());
App.use(bodyParser.json());
App.use(bodyParser.urlencoded({ extended: true }));
App.use(lusca.xframe("SAMEORIGIN"));
App.use(lusca.xssProtection(true));
App.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
App.use(morgan("combined"));
App.use(express.json());
App.use(express.urlencoded({ extended: false }));
App.use(express.static(path.join(__dirname, "public")));
connect("mongodb://mongo:27017/moviesservice", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
const server = App.listen(App.get("port"), () => {
  console.log("App listening on port: " + (process.env.PORT || 4000));
});

MoviesRouter.createMoviesRoutes(App);
CommentsRouter.createCommentsRoutes(App);

export default { App, server };
