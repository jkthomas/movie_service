import { Express, Request, Response } from "express";
import { MoviesController } from "../Controllers/MoviesController";

export namespace MoviesRouter {
  export function createMoviesRoutes(App: Express): void {
    App.get("/movies", MoviesController.getMovies);
    App.post("/movies", MoviesController.acquireMovies);
  }
}
