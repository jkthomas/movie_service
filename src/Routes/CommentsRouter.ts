import { Express, Request, Response } from "express";
import { CommentsController } from "../Controllers/CommentsController";

export namespace CommentsRouter {
  export function createCommentsRoutes(App: Express): void {
    App.get("/comments", CommentsController.getComments);
    App.post("/comments", CommentsController.addComment);
  }
}
