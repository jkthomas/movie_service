interface IResult {
  data: any;
  isSuccessful: boolean;
  errors: string[];
}

export class Result implements IResult {
  data: any = null;
  isSuccessful: boolean = false;
  errors: string[] = [];
}
