import { Result } from "../Lib/App/Result";
import Movie from "../Models/Movie";
import fetch from "node-fetch";

export namespace MoviesService {
  export async function getMovies(): Promise<Result> {
    let result: Result = new Result();
    await Movie.find()
      .exec()
      .then(data => {
        result.isSuccessful = true;
        result.data = { movies: data };
      })
      .catch(e => {
        result.errors = result.errors.concat(e.message);
      });

    return result;
  }

  export async function saveMovies(moviesInput: Object): Promise<Result> {
    let result: Result = new Result();
    await fetch(buildUrl(moviesInput))
      .then(response => response.json())
      .then(async json => {
        if ("Search" in json) {
          const moviesData: Array<{ id: string; title: string }> = [];
          for (let movie of json.Search) {
            const movieEntity = new Movie(movie);
            await movieEntity
              .save()
              .then(data => {
                moviesData.push({ id: data.id, title: data.Title });
              })
              .catch(e => {
                result.errors = result.errors.concat(e.message);
              });
          }
          result.data = { movies: moviesData };
          result.isSuccessful = true;
        } else {
          const movieEntity = new Movie(json);
          await movieEntity
            .save()
            .then(data => {
              result.isSuccessful = true;
              result.data = data.toJSON();
            })
            .catch(e => {
              result.errors = result.errors.concat(e.message);
            });
        }
      })
      .catch(e => {
        result.errors = result.errors.concat(e.message);
      });

    return result;
  }

  function buildUrl(moviesInput: Object): string {
    let url: string = `http://omdbapi.com/`;
    Object.entries(moviesInput).forEach(([parameter, value]) => {
      if (parameter === "apikey") {
        url = url.concat(`?${parameter}=${value}`);
      } else {
        url = url.concat(`&${parameter}=${value}`);
      }
    });
    return url;
  }
}
