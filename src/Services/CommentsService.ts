import { Result } from "../Lib/App/Result";
import Comment from "../Models/Comment";

export namespace CommentsService {
  export async function getComments(): Promise<Result> {
    let result: Result = new Result();
    await Comment.find()
      .exec()
      .then(data => {
        result.isSuccessful = true;
        result.data = data;
      })
      .catch(e => {
        result.errors = result.errors.concat(e.message);
      });

    return result;
  }

  export async function saveComment(commentInput: Object): Promise<Result> {
    let result: Result = new Result();
    const comment = new Comment(commentInput);
    await comment
      .save()
      .then(data => {
        result.isSuccessful = true;
        result.data = data.toJSON();
      })
      .catch(e => {
        result.errors = result.errors.concat(e.message);
      });

    return result;
  }
}
