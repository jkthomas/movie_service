import { Schema, Document, model } from "mongoose";

interface IMovie extends Document {
  Title: string;
  Year: string;
  Rated: string;
  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Country: string;
  Awards: string;
  Poster: string;
  Ratings: string;
  Metascore: string;
  ImdbRating: string;
  ImdbVotes: string;
  imdbID: string;
  Type: string;
  Dvd: string;
  BoxOffice: string;
  Production: string;
  Website: string;
}

const MovieSchema = new Schema({
  Title: { type: String, required: [true, "Title is required"] },
  Year: { type: String, required: [true, "Year is required"] },
  Rated: { type: String, required: [false], default: "N/A" },
  Released: { type: String, required: [false], default: "N/A" },
  Runtime: { type: String, required: [false], default: "N/A" },
  Genre: { type: String, required: [false], default: "N/A" },
  Director: { type: String, required: [false], default: "N/A" },
  Writer: { type: String, required: [false], default: "N/A" },
  Actors: { type: String, required: [false], default: "N/A" },
  Plot: { type: String, required: [false], default: "N/A" },
  Language: { type: String, required: [false], default: "N/A" },
  Country: { type: String, required: [false], default: "N/A" },
  Awards: { type: String, required: [false], default: "N/A" },
  Poster: { type: String, required: [false], default: "N/A" },
  Ratings: {
    type: {
      Source: { type: String, required: [false], default: "N/A" },
      Value: { type: String, required: [false], default: "N/A" }
    },
    required: [false],
    default: "N/A"
  },
  Metascore: { type: String, required: [false], default: "N/A" },
  ImdbRating: { type: String, required: [false], default: "N/A" },
  ImdbVotes: { type: String, required: [false], default: "N/A" },
  imdbID: { type: String, required: [true, "ImdbId is required"] },
  Type: { type: String, required: [true, "Type is required"] },
  Dvd: { type: String, required: [false], default: "N/A" },
  BoxOffice: { type: String, required: [false], default: "N/A" },
  Production: { type: String, required: [false], default: "N/A" },
  Website: { type: String, required: [false], default: "N/A" }
});

export default model<IMovie>("Movie", MovieSchema);
