import { Schema, Document, model } from "mongoose";

interface IComment extends Document {
  username: string;
  content: string;
  movieImdbID: string;
}

const CommentSchema = new Schema({
  username: {
    type: String,
    required: [true, "User name is required"],
    maxlength: [32, "User name can't have more than 32 characters"]
  },
  content: {
    type: String,
    required: [true, "Comment content is required"],
    maxlength: [255, "Comment content can't have more than 255 characters"]
  },
  movieImdbID: {
    type: String,
    required: [true, "Movie IMDB ID is required"]
  }
});

export default model<IComment>("Comment", CommentSchema);
