.PHONY: %
.DEFAULT_GOAL := up

start:
	docker-compose down; docker-compose up

clean:
	docker-compose down

build:
	docker-compose build

dev:
	yarn run dev

test:
	docker exec -it movies sh -c "yarn run test"