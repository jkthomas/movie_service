# Movies service

Simple service for movies

## Getting started

### Linux/MacOS users

First step is having docker environment installed. Next, you need to build application with proper command, while being in project's root directory:

```
make build
```

Command requires elevated user permissions, so you may need to use sudo before command:

```
sudo make build
```

From here, just type in a command which starts docker containers with services:

```
make start
```

Or with super user:

```
sudo make start
```

When you want to clean docker environment use:

```
make clean
```

### Windows users

First, you need Docker for Windows and Compose for Windows with proper configuration on your device. Next, while being in project's root folder run:

```
docker-compose build
```

And then:

```
docker-compose up
```

You need proper permission to do so. Remember to launch Powershell console with administrative privileges.
To clean docker environment:

```
docker-compose down
```

## Testing an application

To launch tests on Linux/MacOS firstly start containers using steps above. Then simply type:

```
make test
```

To launch tests on Windows, you need to exec command:

```
yarn run test
```

In container named movies, when all containers from docker-compose.yml file are running.

## Endpoints

- GET /movies - requires no parameters, returns array of all database movies in object named 'movies'
- POST /movies - requires same parameters as omdbapi ones, available at http://omdbapi.com/ with proper apikey
- GET /comments - requires no parameters, returns array of all database comments in object named 'comments'
- POST /comments - requires 'username', 'content', 'movieImdbID' self-explanatory parameters of type string

In case of missing parameters or incorrect ones, endpoints returns 'errors' object, which is an array of server errors that occured in addition to proper HTTP response codes.
