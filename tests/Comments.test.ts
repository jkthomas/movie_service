import App from "../src/App";
import { Response, Request } from "supertest";
import supertest = require("supertest");
import { disconnect } from "mongoose";

const request = supertest(App.server);
describe("Test comments endpoints", () => {
  it("should fetch and create comment", async done => {
    const comment = {
      username: "test",
      content: "test content",
      movieImdbID: "testid123"
    };
    const result = await request.post("/comments").send(comment);
    expect(result.status).toEqual(201);
    expect(result.badRequest).toEqual(false);
    expect(result.body).toHaveProperty("test");
    expect(result.body).toHaveProperty("test content");
    expect(result.body).toHaveProperty("testid123");
    done();
  });
  it("should return 400 without body", async done => {
    const result = await request.post("/comments");
    expect(result.status).toEqual(400);
    expect(result.badRequest).toEqual(true);
    expect(result.body).toHaveProperty("errors");
    expect(result.body.errors.length).toBeGreaterThan(0);
    expect(result.body.errors[0]).toContain("User name");
    expect(result.body.errors[0]).toContain("Comment content");
    expect(result.body.errors[0]).toContain("required");
    done();
  });
  it("should return 400 with bad parameters", async done => {
    const comment = {
      badParam1: "BadValue1",
      badParam2: "BadValue2"
    };
    const result = await request.post("/comments").send(comment);
    expect(result.status).toEqual(400);
    expect(result.badRequest).toEqual(true);
    expect(result.body).toHaveProperty("errors");
    expect(result.body.errors.length).toBeGreaterThan(0);
    expect(result.body.errors[0]).toContain("User name");
    expect(result.body.errors[0]).toContain("Comment content");
    expect(result.body.errors[0]).toContain("required");
    done();
  });
});

afterAll(async () => {
  try {
    await disconnect();
    await App.server.close();
  } catch (error) {
    console.error(error);
    throw error;
  }
});
