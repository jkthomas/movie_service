import App from "../src/App";
import { Response, Request } from "supertest";
import supertest = require("supertest");
import { disconnect } from "mongoose";

const request = supertest(App.server);
describe("Test movie endpoints", () => {
  it("should fetch and create many movies", async done => {
    const movie = {
      s: "Pulp+Fiction"
    };
    const result = await request.post("/movies").send(movie);
    expect(result.status).toEqual(201);
    expect(result.badRequest).toEqual(false);
    expect(result.body.movies.length).toBeGreaterThan(0);
    expect(result.body.movies.length).toBeGreaterThan(1);
    expect(result.body.movies[0]).toHaveProperty("id");
    expect(result.body.movies[0]).toHaveProperty("title");
    done();
  });
  it("should fetch and create one movie", async done => {
    const movie = {
      t: "Pulp+Fiction"
    };
    const result = await request.post("/movies").send(movie);
    expect(result.status).toEqual(201);
    expect(result.badRequest).toEqual(false);
    expect(result.body).toHaveProperty("Title");
    expect(result.body).toHaveProperty("Actors");
    expect(result.body).toHaveProperty("Ratings");
    done();
  });
  it("should return 400 without body", async done => {
    const result = await request.post("/movies");
    expect(result.status).toEqual(400);
    expect(result.badRequest).toEqual(true);
    expect(result.body).toHaveProperty("errors");
    expect(result.body.errors.length).toBeGreaterThan(0);
    expect(result.body.errors[0]).toContain("Title");
    expect(result.body.errors[0]).toContain("Type");
    expect(result.body.errors[0]).toContain("required");
    done();
  });
  it("should return 400 with bad parameters", async done => {
    const movie = {
      badParam1: "BadValue1",
      badParam2: "BadValue2"
    };
    const result = await request.post("/movies").send(movie);
    expect(result.status).toEqual(400);
    expect(result.badRequest).toEqual(true);
    expect(result.body).toHaveProperty("errors");
    expect(result.body.errors.length).toBeGreaterThan(0);
    done();
  });
});

afterAll(async () => {
  try {
    await disconnect();
    await App.server.close();
  } catch (error) {
    console.error(error);
    throw error;
  }
});
